2020-03-31 - Castelao v3r4
========================================

Based on DV v45r3. This version is released on run2-patches branch.

### Cleanup and testing

- Remove upgrade detectors, !69 (@cattanem)   

### Other

- Prepare Castelao v3r4, !74 (@cvazquez)   
  

- PIDCalib: 2017 reprocessing 2, !73 (@cvazquez)   
  * Add L0Calo tuple tool  
  * Add L0 TISTOS information  
  * Run BremInfo tool only for electrons  
  * Run MuonIDPlus tool only for muons  
  * Fix Lb->Lcmu nu sWeights  
    
  (cherry picked from commit abb27899efeb56db9ac00cab26e525c111d1118f)

- Ignore unchecked status codes, !71 (@clemenci)   
  Port to run2-patches of !70  
    
  As a reminder, some of the unchecked StatusCodes must be checked, but this change has the benefit of preserving the old behaviour.  
    
  See gaudi/Gaudi!763

- Merge branch 'addDummyTest' into 'master', !68 (@cvazquez)   
  Add dummy QMTest  
    
  See merge request lhcb/Castelao!67  
    
  (cherry picked from commit fb36c231bd85d30df541f17ad1eb558f604b5db0)  
    
  33806ba4 Add dummy QMTest  
  a988b741 Update .gitlab-ci.yml  
  160d96dd really add the test  
  c48a1699 Merge branch 'addDummyTest' of https://gitlab.cern.ch:8443/lhcb/Castelao into addDummyTest
