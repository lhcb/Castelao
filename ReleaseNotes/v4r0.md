2021-07-20 Castelao v4r0
===

This version uses DaVinci v45r8, and is released on `run2-patches` branch. It is a major release after adding Python 3 support.

Built relative to Castelao [v3r8](../-/tags/v3r8), with the following changes:

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix clang compilation warnings, !96 (@cattanem)

