2019-12-17 - Castelao v3r2p2
========================================

Based on DaVinci v45r3. This version is released on master branch.

### Cleanup and testing

- First go at removing Run2 specific code, !53 (@sponce)   

### Other

- Scripts for the WGP of neutral samples, !60 (@chefdevi)   
  This is a new branch based on Castelao v3r2p1 (the previous one had too many unnecessary files). Contains fit models, offline cuts and a configuration file for the MDST and Tuple productions.

- Revert "sPlot of neutral lines", !58 (@cvazquez)   
  This reverts commit e2e55ce06966d0a22d19f978c60596defc21ba67

- Revert "sPlot of neutral lines", !57 (@cvazquez)   
  This reverts commit 166d5447c2f4df3b7007da51e534bcd916984300

