###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
DSTARCUT="(abs(Dst_LOKI_MM-D0_LOKI_MM-145.42)<5.0)"


for path in "$@"
do

fname=$(basename $path .root)

passname="_TightDStar.root"

wsname="_WS"
signame="_Signal"
npname="_NP"

dname="/data/lhcb/users/hill/gangadir/workspace/dhill/LocalXML/425/tuple_sum/"

cutapplier $path Bd2DsttaunuTuple/DecayTree "$DSTARCUT"  $dname/$fname$signame$passname
cutapplier $path Bd2DsttaunuWSTuple/DecayTree "$DSTARCUT"  $dname/$fname$wsname$passname
cutapplier $path Bd2DsttaunuNonPhysTuple/DecayTree "$DSTARCUT"  $dname/$fname$npname$passname

done
