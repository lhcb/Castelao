###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os
import sys
import inspect
from B2XTauNuTools.TupleHelper import *

# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

outputLevel = INFO

SeqTupleBd2Dsttaunu = GaudiSequencer("SeqTupleBd2Dsttaunu")
TupleBd2Dsttaunu = DecayTreeTuple("Bd2DsttaunuTuple")
TupleBd2Dsttaunu.OutputLevel = outputLevel
TupleBd2Dsttaunu.Inputs = ["/Event/Phys/Bd2DstarTauNuForB2XTauNu/Particles"]
TupleBd2Dsttaunu.Decay = "[B0 -> (^D*(2010)- -> ^pi- (^D~0 -> ^K+ ^pi- )) (^tau+ -> ^pi+ ^pi- ^pi+ )]cc"
TupleBd2Dsttaunu.Branches = {
    "B0" : "[B0]cc : [B0 -> (D*(2010)- -> pi- (D~0 -> K+ pi- )) (tau+ -> pi+ pi- pi+)]cc",
    "tau_pion" : "[B0 -> (D*(2010)- -> pi- (D~0 -> K+ pi- )) (tau+ -> ^pi+ ^pi- ^pi+)]cc",
    "dz_pion" : "[B0 -> (D*(2010)- -> pi- (D~0 -> K+ ^pi- )) (tau+ -> pi+ pi- pi+)]cc",
    "dst_pion" : "[B0 -> (D*(2010)- -> ^pi- (D~0 -> K+ pi- )) (tau+ -> pi+ pi- pi+)]cc",
    "dz_kaon" : "[B0 -> (D*(2010)- -> pi- (D~0 -> ^K+ pi- )) (tau+ -> pi+ pi- pi+)]cc",
    "tau" : "[B0 -> (D*(2010)- -> pi- (D~0 -> K+ pi- )) (^tau+ -> pi+ pi- pi+)]cc",
    "D0" : "[B0 -> (D*(2010)- -> pi- (^D~0 -> K+ pi- )) (tau+ -> pi+ pi- pi+)]cc",
    "Dst" : "[B0 -> (^D*(2010)- -> pi- (D~0 -> K+ pi- )) (tau+ -> pi+ pi- pi+)]cc"}


TupleBd2Dsttaunu.addTool(TupleToolDecay, name='dst_pion')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='tau_pion')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='dz_pion')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='dz_kaon')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='tau')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='D0')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='Dst')
TupleBd2Dsttaunu.addTool(TupleToolDecay, name='B0')

FinalStateConfig(TupleBd2Dsttaunu.dst_pion)
FinalStateConfig(TupleBd2Dsttaunu.tau_pion)
FinalStateConfig(TupleBd2Dsttaunu.dz_pion)
FinalStateConfig(TupleBd2Dsttaunu.dz_kaon)
DalitzConfig(TupleBd2Dsttaunu.tau)   #,True)
IsoConfig(TupleBd2Dsttaunu.tau)  #,True)
IsoConfig(TupleBd2Dsttaunu.B0)   #,True)
#LifetimeConfig(TupleBd2Dsttaunu.tau)
ParentConfig(TupleBd2Dsttaunu.tau)
ParentConfig(TupleBd2Dsttaunu.Dst)
ParentConfig(TupleBd2Dsttaunu.D0)
ParentConfig(TupleBd2Dsttaunu.B0)
CommonConfig(TupleBd2Dsttaunu.dst_pion)
CommonConfig(TupleBd2Dsttaunu.tau_pion)
CommonConfig(TupleBd2Dsttaunu.dz_pion)
KinematicFitConfig(TupleBd2Dsttaunu.B0,True)

CommonConfig(TupleBd2Dsttaunu.tau)
CommonConfig(TupleBd2Dsttaunu.Dst)
CommonConfig(TupleBd2Dsttaunu.D0)
CommonConfig(TupleBd2Dsttaunu.B0)
CommonConfig(TupleBd2Dsttaunu.dz_kaon)
TISTOSToolConfig(TupleBd2Dsttaunu.B0)
TupleToolConfig(TupleBd2Dsttaunu)
TupleToolMCConfig(TupleBd2Dsttaunu)
GaudiSequencer("SeqTupleBd2Dsttaunu").Members.append(TupleBd2Dsttaunu)
SeqTupleBd2Dsttaunu.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBd2Dsttaunu").Members.append(SeqTupleBd2Dsttaunu)
