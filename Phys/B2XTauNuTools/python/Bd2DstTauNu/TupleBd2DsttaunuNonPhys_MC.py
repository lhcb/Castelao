###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os
import sys
import inspect
from B2XTauNuTools.TupleHelper import *

# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

outputLevel = INFO

SeqTupleBd2DsttaunuNonPhys = GaudiSequencer("SeqTupleBd2DsttaunuNonPhys")
TupleBd2DsttaunuNonPhys = DecayTreeTuple("Bd2DsttaunuNonPhysTuple")
TupleBd2DsttaunuNonPhys.OutputLevel = outputLevel
TupleBd2DsttaunuNonPhys.Inputs = ["/Event/Phys/Bd2DstarTauNuNonPhysTauForB2XTauNu/Particles"]
TupleBd2DsttaunuNonPhys.Decay = "[B0 -> (^D*(2010)- -> ^pi- (^D~0 -> ^K+ ^pi- )) (^tau+ -> ^pi+ ^pi+ ^pi+ )]cc"
TupleBd2DsttaunuNonPhys.Branches = {
    "B0" : "[B0]cc : [B0 -> (D*(2010)- -> pi- (D~0 -> K+ pi- )) (tau+ -> pi+ pi+ pi+)]cc",
    "tau_pion" : "[B0 -> (D*(2010)- -> pi- (D~0 -> K+ pi- )) (tau+ -> ^pi+ ^pi+ ^pi+)]cc",
    "dz_pion" : "[B0 -> (D*(2010)- -> pi- (D~0 -> K+ ^pi- )) (tau+ -> pi+ pi+ pi+)]cc",
    "dst_pion" : "[B0 -> (D*(2010)- -> ^pi- (D~0 -> K+ pi- )) (tau+ -> pi+ pi+ pi+)]cc",
    "dz_kaon" : "[B0 -> (D*(2010)- -> pi- (D~0 -> ^K+ pi- )) (tau+ -> pi+ pi+ pi+)]cc",
    "tau" : "[B0 -> (D*(2010)- -> pi- (D~0 -> K+ pi- )) (^tau+ -> pi+ pi+ pi+)]cc",
    "D0" : "[B0 -> (D*(2010)- -> pi- (^D~0 -> K+ pi- )) (tau+ -> pi+ pi+ pi+)]cc",
    "Dst" : "[B0 -> (^D*(2010)- -> pi- (D~0 -> K+ pi- )) (tau+ -> pi+ pi+ pi+)]cc"}


TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='dst_pion')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='tau_pion')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='dz_pion')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='dz_kaon')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='tau')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='D0')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='Dst')
TupleBd2DsttaunuNonPhys.addTool(TupleToolDecay, name='B0')

FinalStateConfig(TupleBd2DsttaunuNonPhys.dst_pion)
FinalStateConfig(TupleBd2DsttaunuNonPhys.tau_pion)
FinalStateConfig(TupleBd2DsttaunuNonPhys.dz_pion)
FinalStateConfig(TupleBd2DsttaunuNonPhys.dz_kaon)
DalitzConfig(TupleBd2DsttaunuNonPhys.tau)
IsoConfig(TupleBd2DsttaunuNonPhys.tau)   #,True)
IsoConfig(TupleBd2DsttaunuNonPhys.B0)   #,True)
#LifetimeConfig(TupleBd2DsttaunuNonPhys.tau)
ParentConfig(TupleBd2DsttaunuNonPhys.tau)
ParentConfig(TupleBd2DsttaunuNonPhys.Dst)
ParentConfig(TupleBd2DsttaunuNonPhys.D0)
ParentConfig(TupleBd2DsttaunuNonPhys.B0)
CommonConfig(TupleBd2DsttaunuNonPhys.dst_pion)
CommonConfig(TupleBd2DsttaunuNonPhys.tau_pion)
CommonConfig(TupleBd2DsttaunuNonPhys.dz_pion)
KinematicFitConfig(TupleBd2DsttaunuNonPhys.B0,True)


CommonConfig(TupleBd2DsttaunuNonPhys.tau)
CommonConfig(TupleBd2DsttaunuNonPhys.Dst)
CommonConfig(TupleBd2DsttaunuNonPhys.D0)
CommonConfig(TupleBd2DsttaunuNonPhys.B0)
CommonConfig(TupleBd2DsttaunuNonPhys.dz_kaon)
TISTOSToolConfig(TupleBd2DsttaunuNonPhys.B0)
TupleToolConfig(TupleBd2DsttaunuNonPhys)
TupleToolMCConfig(TupleBd2DsttaunuNonPhys)
GaudiSequencer("SeqTupleBd2DsttaunuNonPhys").Members.append(TupleBd2DsttaunuNonPhys)
SeqTupleBd2DsttaunuNonPhys.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBd2DsttaunuNonPhys").Members.append(SeqTupleBd2DsttaunuNonPhys)
