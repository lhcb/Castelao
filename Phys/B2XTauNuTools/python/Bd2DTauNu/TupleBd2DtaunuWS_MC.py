###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBd2DtaunuWS = GaudiSequencer("SeqTupleBd2DtaunuWS")
TupleBd2DtaunuWS = DecayTreeTuple("Bd2DtaunuWSTuple")
TupleBd2DtaunuWS.OutputLevel = outputLevel
TupleBd2DtaunuWS.Inputs = ["/Event/Phys/Bd2DTauNuWSForB2XTauNu/Particles"]
TupleBd2DtaunuWS.Decay = "[B0 -> (^D+ -> ^K- ^pi+ ^pi+) (^tau+ -> ^pi+ ^pi- ^pi+ )]cc"
TupleBd2DtaunuWS.Branches = {
    "B0" : "[B0]cc : [B0 -> (D+ -> K- pi+ pi+) (tau+ -> pi+ pi- pi+)]cc",
    "tau_pion" : "[B0 -> (D+ -> K- pi+ pi+) (tau+ -> ^pi+ ^pi- ^pi+)]cc",
    "d_pion" : "[B0 -> (D+ -> K- ^pi+ ^pi+) (tau+ -> pi+ pi- pi+)]cc",
    "d_kaon" : "[B0 -> (D+ -> ^K- pi+ pi+) (tau+ -> pi+ pi- pi+)]cc",
    "tau" : "[B0 -> (D+ -> K- pi+ pi+) (^tau+ -> pi+ pi- pi+)]cc",
    "D" : "[B0 -> (^D+ -> K- pi+ pi+) (tau+ -> pi+ pi- pi+)]cc"}


TupleBd2DtaunuWS.addTool(TupleToolDecay, name = 'tau_pion')
TupleBd2DtaunuWS.addTool(TupleToolDecay, name = 'd_pion')
TupleBd2DtaunuWS.addTool(TupleToolDecay, name = 'd_kaon')
TupleBd2DtaunuWS.addTool(TupleToolDecay, name = 'tau')
TupleBd2DtaunuWS.addTool(TupleToolDecay, name = 'D')
TupleBd2DtaunuWS.addTool(TupleToolDecay, name = 'B0')

FinalStateConfig(TupleBd2DtaunuWS.tau_pion)
FinalStateConfig(TupleBd2DtaunuWS.d_pion)
FinalStateConfig(TupleBd2DtaunuWS.d_kaon)
#DalitzConfig(TupleBd2DtaunuWS.tau)
LifetimeConfig(TupleBd2DtaunuWS.tau)

ParentConfig(TupleBd2DtaunuWS.tau)
ParentConfig(TupleBd2DtaunuWS.D)
ParentConfig(TupleBd2DtaunuWS.B0)
CommonConfig(TupleBd2DtaunuWS.tau_pion)
CommonConfig(TupleBd2DtaunuWS.d_pion)

CommonConfig(TupleBd2DtaunuWS.tau)
CommonConfig(TupleBd2DtaunuWS.D)
CommonConfig(TupleBd2DtaunuWS.B0)
CommonConfig(TupleBd2DtaunuWS.d_kaon)
TISTOSToolConfig(TupleBd2DtaunuWS.B0)

TupleToolConfig(TupleBd2DtaunuWS)
TupleToolMCConfig(TupleBd2DtaunuWS)

GaudiSequencer("SeqTupleBd2DtaunuWS").Members.append(TupleBd2DtaunuWS)

SeqTupleBd2DtaunuWS.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBd2DtaunuWS").Members.append(SeqTupleBd2DtaunuWS)

