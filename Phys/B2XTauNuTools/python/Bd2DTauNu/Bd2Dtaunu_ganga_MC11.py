###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
dv = DaVinci()
dv.version = 'v33r2'
dv.platform = 'x86_64-slc5-gcc43-opt'
base=dv.user_release_area+'/DaVinci_'+dv.version+'/Phys/B2XTauNuTools/python/Bd2DTauNu_MC/'
dv.optsfile = [base+'Bd2Dtaunu_strp_MC11.py']

myJobs = [
	 ['Bd2DtaunuMC11a','MCMC11a11436000Beam3500GeV-2011-MagDown-Nu2-EmNoCutsSim05bTrig0x40760037FlaggedReco12aStripping17NoPrescalingFlaggedSTREAMSDST.py']
	#,['Bd2DDsMC11a','MCMC11a11836001Beam3500GeV-2011-MagUp-Nu2-EmNoCutsSim05bTrig0x40760037FlaggedReco12aStripping17NoPrescalingFlaggedSTREAMSDST.py']
	,['Bd2D3piMC11a','MCMC11a11266006Beam3500GeV-2011-MagDown-Nu2-EmNoCutsSim05Trig0x40760037FlaggedReco12aStripping17FlaggedSTREAMSDST.py']
]

for i in myJobs:	

	j = Job (name=i[0],application=dv)

	j.backend          = Dirac()
	j.inputdata        = dv.readInputData(base+i[1])
	j.splitter         = SplitByFiles(filesPerJob=10)
	j.outputfiles      = ['DVNtuple.root','DVHistos.root']
	j.postprocessors   = RootMerger(overwrite=True,ignorefailed=True,files=['DVNtuple.root'])
	j.do_auto_resubmit = True
	j.submit()
