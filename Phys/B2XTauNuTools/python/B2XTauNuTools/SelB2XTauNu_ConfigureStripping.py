###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#Get the original stripping line candidates
from Configurables import (
    ProcStatusCheck,
    DaVinci,
    EventNodeKiller
)
from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingStream import StrippingStream
from StrippingSelections.StrippingSL import StrippingB2XTauNu

#Kill old stripping
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = ['/Event/AllStreams',
                         '/Event/Strip']

DaVinci().appendToMainSequence([eventNodeKiller])   # Kill old stripping banks first

# Now build the StrippingStreamB2XTauNu
StrippingStreamB2XTauNu = StrippingStream("StrippingStreamB2XTauNu")
ConfB2XTauNu = StrippingB2XTauNu.B2XTauNuAllLinesConf(
    name="B2XTauNu",
    config=StrippingB2XTauNu.default_config['CONFIG']
)

StrippingStreamB2XTauNu.appendLines(ConfB2XTauNu.lines())

# Standard configuration of Stripping
MCStrippingConfB2XTauNu = StrippingConf(
    Streams = [StrippingStreamB2XTauNu],
    MaxCandidates = 2000,
    AcceptBadEvents = False,
    BadEventSelection = ProcStatusCheck(),
    TESPrefix = 'Strip'
)

DaVinci().appendToMainSequence([MCStrippingConfB2XTauNu.sequence()])

