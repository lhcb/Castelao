###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configuration script for the tuple tools used in B2XTauNu analyses.
'''
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (
    OfflineVertexFitter,
    DecayTreeTuple,
    TupleToolGeneration,
    TupleToolMCTruth,
    TupleToolMCBackgroundInfo,
    TupleToolRecoStats,
    TupleB2XMother,
    TupleToolGeometry,
    TupleToolKinematic,
    TupleToolPrimaries,
    TupleToolEventInfo,
    TupleToolDalitz,
    TupleToolPid,
    TupleToolTrackInfo,
    TupleToolVtxIsoln,
    TupleToolTagging,
    TupleToolTrigger,
    TupleToolEWTrackIsolation,
    TupleToolDecay,
    TupleToolTISTOS,
    MCTupleToolKinematic,
    MCTupleToolHierarchy,
    LoKi__Hybrid__TupleTool,
    PrintHeader
)
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from DecayTreeTuple.Configuration import *

outputLevel = INFO

triggers_run1 = [
    'Hlt1DiMuonHighMassDecision'
    'Hlt1DiMuonLowMassDecision',
    'Hlt1SingleMuonNoIPDecision',
    'Hlt1SingleMuonHighPTDecision',
    'Hlt1SingleElectronNoIPDecision',
    'Hlt1TrackAllL0Decision',
    'Hlt1TrackMuonDecision',
    'Hlt1TrackPhotonDecision',
    'Hlt1TrackForwardPassThroughDecision',
    'Hlt1TrackForwardPassThroughLooseDecision',
    'Hlt1LumiDecision',
    'Hlt1LumiMidBeamCrossingDecision',
    'Hlt1MBNoBiasDecision',
    'Hlt1CharmCalibrationNoBiasDecision',
    'Hlt1MBMicroBiasVeloDecision',
    'Hlt1MBMicroBiasVeloRateLimitedDecision',
    'Hlt1MBMicroBiasTStationDecision',
    'Hlt1MBMicroBiasTStationRateLimitedDecision',
    'Hlt1L0AnyDecision',
    'Hlt1L0AnyRateLimitedDecision',
    'Hlt1L0AnyNoSPDDecision',
    'Hlt1L0AnyNoSPDRateLimitedDecision',
    'Hlt1L0HighSumETJetDecision',
    'Hlt1NoPVPassThroughDecision',
    'Hlt1DiProtonDecision',
    'Hlt1DiProtonLowMultDecision',
    'Hlt1BeamGasNoBeamBeam1Decision',
    'Hlt1BeamGasNoBeamBeam2Decision',
    'Hlt1BeamGasBeam1Decision',
    'Hlt1BeamGasBeam2Decision',
    'Hlt1BeamGasCrossingEnhancedBeam1Decision',
    'Hlt1BeamGasCrossingEnhancedBeam2Decision',
    'Hlt1BeamGasCrossingForcedRecoDecision',
    'Hlt1BeamGasCrossingForcedRecoFullZDecision',
    'Hlt1BeamGasHighRhoVerticesDecision',
    'Hlt1ODINTechnicalDecision',
    'Hlt1Tell1ErrorDecision',
    'Hlt1VeloClosingMicroBiasDecision',
    'Hlt1VertexDisplVertexDecision',
    'Hlt1BeamGasCrossingParasiticDecision',
    'Hlt1ErrorEventDecision',
    'Hlt1GlobalDecision',
    'Hlt2B2HHPi0_MergedDecision',
    'Hlt2DiMuonJPsiDecision',
    'Hlt2DiMuonJPsiHighPTDecision',
    'Hlt2DiMuonPsi2SDecision',
    'Hlt2DiMuonPsi2SHighPTDecision',
    'Hlt2DiMuonBDecision',
    'Hlt2DiMuonZDecision',
    'Hlt2DiMuonDY1Decision',
    'Hlt2DiMuonDY2Decision',
    'Hlt2DiMuonDY3Decision',
    'Hlt2DiMuonDY4Decision',
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedHeavyDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2TriMuonDetachedDecision',
    'Hlt2DoubleDiMuonDecision',
    'Hlt2DiMuonAndMuonDecision',
    'Hlt2TriMuonTauDecision',
    'Hlt2DiMuonAndGammaDecision',
    'Hlt2DiMuonAndD0Decision',
    'Hlt2DiMuonAndDpDecision',
    'Hlt2DiMuonAndDsDecision',
    'Hlt2DiMuonAndLcDecision',
    'Hlt2SingleTFElectronDecision',
    'Hlt2SingleElectronTFLowPtDecision',
    'Hlt2SingleElectronTFHighPtDecision',
    'Hlt2SingleTFVHighPtElectronDecision',
    'Hlt2DiElectronHighMassDecision',
    'Hlt2DiElectronBDecision',
    'Hlt2B2HHLTUnbiasedDecision',
    'Hlt2B2HHLTUnbiasedDetachedDecision',
    'Hlt2Topo2BodySimpleDecision',
    'Hlt2Topo3BodySimpleDecision',
    'Hlt2Topo4BodySimpleDecision',
    'Hlt2Topo2BodyBBDTDecision',
    'Hlt2Topo3BodyBBDTDecision',
    'Hlt2Topo4BodyBBDTDecision',
    'Hlt2TopoMu2BodyBBDTDecision',
    'Hlt2TopoMu3BodyBBDTDecision',
    'Hlt2TopoMu4BodyBBDTDecision',
    'Hlt2TopoE2BodyBBDTDecision',
    'Hlt2TopoE3BodyBBDTDecision',
    'Hlt2TopoE4BodyBBDTDecision',
    'Hlt2TopoRad2BodyBBDTDecision',
    'Hlt2TopoRad2plus1BodyBBDTDecision',
    'Hlt2IncPhiDecision',
    'Hlt2IncPhiSidebandsDecision',
    'Hlt2CharmHadD02HHKsLLDecision',
    'Hlt2CharmHadD02HHKsDDDecision',
    'Hlt2Dst2PiD02PiPiDecision',
    'Hlt2Dst2PiD02MuMuDecision',
    'Hlt2Dst2PiD02KMuDecision',
    'Hlt2Dst2PiD02KPiDecision',
    'Hlt2PassThroughDecision',
    'Hlt2TransparentDecision',
    'Hlt2LumiDecision',
    'Hlt2ForwardDecision',
    'Hlt2DebugEventDecision',
    'Hlt2CharmHadD2KS0KS0Decision',
    'Hlt2CharmHadD2KS0KS0WideMassDecision',
    'Hlt2CharmHadD02HH_D02PiPiDecision',
    'Hlt2CharmHadD02HH_D02PiPiWideMassDecision',
    'Hlt2CharmHadD02HH_D02KKDecision',
    'Hlt2CharmHadD02HH_D02KKWideMassDecision',
    'Hlt2CharmHadD02HH_D02KPiDecision',
    'Hlt2CharmHadD02HH_D02KPiWideMassDecision',
    'Hlt2ExpressJPsiDecision',
    'Hlt2ExpressJPsiTagProbeDecision',
    'Hlt2ExpressLambdaDecision',
    'Hlt2ExpressKSDecision',
    'Hlt2ExpressDs2PhiPiDecision',
    'Hlt2ExpressBeamHaloDecision',
    'Hlt2ExpressDStar2D0PiDecision',
    'Hlt2ExpressD02KPiDecision',
    'Hlt2CharmHadLambdaC2KPPiDecision',
    'Hlt2CharmHadLambdaC2KPPiWideMassDecision',
    'Hlt2CharmHadLambdaC2KPKDecision',
    'Hlt2CharmHadLambdaC2KPKWideMassDecision',
    'Hlt2CharmHadLambdaC2PiPPiDecision',
    'Hlt2CharmHadLambdaC2PiPPiWideMassDecision',
    'Hlt2CharmHadLambdaC2PiPKDecision',
    'Hlt2CharmHadLambdaC2PiPKWideMassDecision',
    'Hlt2Bs2PhiGammaDecision',
    'Hlt2Bs2PhiGammaWideBMassDecision',
    'Hlt2Bd2KstGammaDecision',
    'Hlt2Bd2KstGammaWideKMassDecision',
    'Hlt2Bd2KstGammaWideBMassDecision',
    'Hlt2CharmHadD2KS0H_D2KS0PiDecision',
    'Hlt2CharmHadD2KS0H_D2KS0KDecision',
    'Hlt2CharmHadD2KS0H_D2KS0DDPiDecision',
    'Hlt2CharmHadD2KS0H_D2KS0DDKDecision',
    'Hlt2DiPhiDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_4piDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_4piWideMassDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_K3piDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_K3piWideMassDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_KKpipiDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_KKpipiWideMassDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_2K2piDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_2K2piWideMassDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_3KpiDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_3KpiWideMassDecision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_Ch2Decision',
    'Hlt2CharmHadD02HHHHDstNoHltOne_Ch2WideMassDecision',
    'Hlt2CharmRareDecayD02MuMuDecision',
    'Hlt2B2HHDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2SingleMuonHighPTDecision',
    'Hlt2SingleMuonVHighPTDecision',
    'Hlt2SingleMuonLowPTDecision',
    'Hlt2DiProtonDecision',
    'Hlt2DiProtonLowMultDecision',
    'Hlt2CharmSemilepD02HMuNu_D02KMuNuWSDecision',
    'Hlt2CharmSemilepD02HMuNu_D02PiMuNuWSDecision',
    'Hlt2CharmSemilepD02HMuNu_D02KMuNuDecision',
    'Hlt2CharmSemilepD02HMuNu_D02KMuNuTightDecision',
    'Hlt2CharmSemilepD02HMuNu_D02PiMuNuDecision',
    'Hlt2CharmHadMinBiasLambdaC2KPPiDecision',
    'Hlt2CharmHadMinBiasD02KPiDecision',
    'Hlt2CharmHadMinBiasD02KKDecision',
    'Hlt2CharmHadMinBiasDplus2hhhDecision',
    'Hlt2CharmHadMinBiasLambdaC2LambdaPiDecision',
    'Hlt2TFBc2JpsiMuXDecision',
    'Hlt2TFBc2JpsiMuXSignalDecision',
    'Hlt2diPhotonDiMuonDecision',
    'Hlt2LowMultMuonDecision',
    'Hlt2LowMultHadronDecision',
    'Hlt2LowMultHadron_nofilterDecision',
    'Hlt2LowMultPhotonDecision',
    'Hlt2LowMultElectronDecision',
    'Hlt2LowMultElectron_nofilterDecision',
    'Hlt2DisplVerticesHighMassSingleDecision',
    'Hlt2DisplVerticesDoubleDecision',
    'Hlt2DisplVerticesHighFDSingleDecision',
    'Hlt2DisplVerticesSingleDecision',
    'Hlt2DisplVerticesSinglePostScaledDecision',
    'Hlt2DisplVerticesDoublePostScaledDecision',
    'Hlt2DisplVerticesSingleHighMassPostScaledDecision',
    'Hlt2DisplVerticesSingleHighFDPostScaledDecision',
    'Hlt2DisplVerticesSingleMVPostScaledDecision',
    'Hlt2DisplVerticesSingleDownDecision',
    'Hlt2CharmSemilep3bodyD2PiMuMuDecision',
    'Hlt2CharmSemilep3bodyD2PiMuMuSSDecision',
    'Hlt2CharmSemilep3bodyD2KMuMuDecision',
    'Hlt2CharmSemilep3bodyD2KMuMuSSDecision',
    'Hlt2CharmSemilep3bodyLambdac2PMuMuDecision',
    'Hlt2CharmSemilep3bodyLambdac2PMuMuSSDecision',
    'Hlt2LambdaC_LambdaC2Lambda0LLPiDecision',
    'Hlt2LambdaC_LambdaC2Lambda0LLKDecision',
    'Hlt2LambdaC_LambdaC2Lambda0DDPiDecision',
    'Hlt2LambdaC_LambdaC2Lambda0DDKDecision',
    'Hlt2RadiativeTopoTrackDecision',
    'Hlt2RadiativeTopoPhotonDecision',
    'Hlt2CharmHadD2HHHDecision',
    'Hlt2CharmHadD2HHHWideMassDecision',
    'Hlt2CharmHadD02HHHHDst_4piDecision',
    'Hlt2CharmHadD02HHHHDst_4piWideMassDecision',
    'Hlt2CharmHadD02HHHHDst_K3piDecision',
    'Hlt2CharmHadD02HHHHDst_K3piWideMassDecision',
    'Hlt2CharmHadD02HHHHDst_KKpipiDecision',
    'Hlt2CharmHadD02HHHHDst_KKpipiWideMassDecision',
    'Hlt2CharmHadD02HHHHDst_2K2piDecision',
    'Hlt2CharmHadD02HHHHDst_2K2piWideMassDecision',
    'Hlt2CharmHadD02HHHHDst_3KpiDecision',
    'Hlt2CharmHadD02HHHHDst_3KpiWideMassDecision',
    'Hlt2CharmHadD02HHHHDst_Ch2Decision',
    'Hlt2CharmHadD02HHHHDst_Ch2WideMassDecision',
    'Hlt2CharmSemilepD02PiPiMuMuDecision',
    'Hlt2CharmSemilepD02KKMuMuDecision',
    'Hlt2CharmSemilepD02KPiMuMuDecision',
    'Hlt2CharmHadD02HHHH_4piDecision',
    'Hlt2CharmHadD02HHHH_4piWideMassDecision',
    'Hlt2CharmHadD02HHHH_K3piDecision',
    'Hlt2CharmHadD02HHHH_K3piWideMassDecision',
    'Hlt2CharmHadD02HHHH_KKpipiDecision',
    'Hlt2CharmHadD02HHHH_KKpipiWideMassDecision',
    'Hlt2CharmHadD02HHHH_2K2piDecision',
    'Hlt2CharmHadD02HHHH_2K2piWideMassDecision',
    'Hlt2CharmHadD02HHHH_3KpiDecision',
    'Hlt2CharmHadD02HHHH_3KpiWideMassDecision',
    'Hlt2CharmHadD02HHHH_Ch2Decision',
    'Hlt2CharmHadD02HHHH_Ch2WideMassDecision',
    'Hlt2ErrorEventDecision',
    'Hlt2GlobalDecision',
    'L0B1gasDecision',
    'L0B2gasDecision',
    'L0CALODecision',
    'L0DiEM,lowMultDecision',
    'L0DiHadron,lowMultDecision',
    'L0DiMuonDecision',
    'L0DiMuon,lowMultDecision',
    'L0DiMuonNoSPDDecision',
    'L0ElectronDecision',
    'L0Electron,lowMultDecision',
    'L0ElectronHiDecision',
    'L0ElectronNoSPDDecision',
    'L0HadronDecision',
    'L0HadronNoSPDDecision',
    'L0HighSumETJetDecision',
    'L0MUON,minbiasDecision',
    'L0MuonDecision',
    'L0Muon,lowMultDecision',
    'L0MuonNoSPDDecision',
    'L0NoPVFlagDecision',
    'L0PhotonDecision',
    'L0Photon,lowMultDecision',
    'L0PhotonHiDecision',
    'L0PhotonNoSPDDecision',
    ]

triggers_run2 = [
    'Hlt2B2HH_B2HHDecision',
    'Hlt2B2HH_B2KKDecision',
    'Hlt2B2HH_B2KPiDecision',
    'Hlt2B2HH_B2PiPiDecision',
    'Hlt2B2HH_Lb2PKDecision',
    'Hlt2B2HH_Lb2PPiDecision',
    'Hlt2B2Kpi0_B2K0pi0Decision',
    'Hlt2B2Kpi0_B2Kpi0Decision',
    'Hlt2BHadB02PpPpPmPmDecision',
    'Hlt2Bc2JpsiXTFBc2JpsiMuXDecision',
    'Hlt2BeamGasDecision',
    'Hlt2BottomoniumDiPhiDecision',
    'Hlt2CcDiHadronDiPhiDecision',
    'Hlt2CcDiHadronDiProtonDecision',
    'Hlt2CcDiHadronDiProtonLowMultDecision',
    'Hlt2CharmHadDp2EtaKp_Eta2EmEpGDecision',
    'Hlt2CharmHadDp2EtaKp_Eta2PimPipGDecision',
    'Hlt2CharmHadDp2EtaKp_Eta2PimPipPi0_Pi0MDecision',
    'Hlt2CharmHadDp2EtaKp_Eta2PimPipPi0_Pi0RDecision',
    'Hlt2CharmHadDp2EtaPip_Eta2EmEpGDecision',
    'Hlt2CharmHadDp2EtaPip_Eta2PimPipGDecision',
    'Hlt2CharmHadDp2EtaPip_Eta2PimPipPi0_Pi0MDecision',
    'Hlt2CharmHadDp2EtaPip_Eta2PimPipPi0_Pi0RDecision',
    'Hlt2CharmHadDp2EtapKp_Etap2EtaPimPip_EtaRDecision',
    'Hlt2CharmHadDp2EtapKp_Etap2PimPipGDecision',
    'Hlt2CharmHadDp2EtapPip_Etap2EtaPimPip_EtaRDecision',
    'Hlt2CharmHadDp2EtapPip_Etap2PimPipGDecision',
    'Hlt2CharmHadDp2KpPi0_Pi02EmEpGDecision',
    'Hlt2CharmHadDp2PipPi0_Pi02EmEpGDecision',
    'Hlt2CharmHadDpDsp2KmKpKpPi0Decision',
    'Hlt2CharmHadDpDsp2KmKpPipPi0Decision',
    'Hlt2CharmHadDpDsp2KmPipPipPi0Decision',
    'Hlt2CharmHadDpDsp2KpKpPimPi0Decision',
    'Hlt2CharmHadDpDsp2KpPimPipPi0Decision',
    'Hlt2CharmHadDpDsp2PimPipPipPi0Decision',
    'Hlt2CharmHadDspToKmKpPipDecision',
    'Hlt2CharmHadDstp2D0Pip_D02EmEpDecision',
    'Hlt2CharmHadDstp2D0Pip_D02GG_G2EmEpDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0MDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0RDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0MDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0RDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0MDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0RDecision',
    'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0MDecision',
    'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0RDecision',
    'Hlt2CharmHadInclDst2PiD02HHXBDTDecision',
    'Hlt2CharmHadInclSigc2PiLc2HHXBDTDecision',
    'Hlt2DPS2muHcDecision',
    'Hlt2DPS2x2muDecision',
    'Hlt2DPS2xHcDecision',
    'Hlt2DebugEventDecision',
    'Hlt2DiElectronElSoftDecision',
    'Hlt2DiMuonBDecision',
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedHeavyDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2DiMuonDetachedPsi2SDecision',
    'Hlt2DiMuonJPsiDecision',
    'Hlt2DiMuonJPsiHighPTDecision',
    'Hlt2DiMuonPsi2SDecision',
    'Hlt2DiMuonPsi2SHighPTDecision',
    'Hlt2DiMuonSoftDecision',
    'Hlt2DiMuonZDecision',
    'Hlt2DisplVerticesDoubleDecision',
    'Hlt2DisplVerticesDoublePSDecision',
    'Hlt2DisplVerticesSingleDecision',
    'Hlt2DisplVerticesSingleHighFDDecision',
    'Hlt2DisplVerticesSingleHighMassDecision',
    'Hlt2DisplVerticesSingleLoosePSDecision',
    'Hlt2DisplVerticesSinglePSDecision',
    'Hlt2DisplVerticesSingleVeryHighFDDecision',
    'Hlt2EWDiElectronDYDecision',
    'Hlt2EWDiElectronHighMassDecision',
    'Hlt2EWDiMuonDY1Decision',
    'Hlt2EWDiMuonDY2Decision',
    'Hlt2EWDiMuonDY3Decision',
    'Hlt2EWDiMuonDY4Decision',
    'Hlt2EWDiMuonZDecision',
    'Hlt2EWSingleElectronHighPtDecision',
    'Hlt2EWSingleElectronLowPtDecision',
    'Hlt2EWSingleElectronVHighPtDecision',
    'Hlt2EWSingleMuonHighPtDecision',
    'Hlt2EWSingleMuonLowPtDecision',
    'Hlt2EWSingleMuonVHighPtDecision',
    'Hlt2EWSingleTauHighPt2ProngDecision',
    'Hlt2EWSingleTauHighPt3ProngDecision',
    'Hlt2ExoticaDiRHNuDecision',
    'Hlt2ExoticaDisplDiMuonDecision',
    'Hlt2ExoticaDisplDiMuonNoPointDecision',
    'Hlt2ExoticaDisplPhiPhiDecision',
    'Hlt2ExoticaPrmptDiMuonHighMassDecision',
    'Hlt2ExoticaQuadMuonNoIPDecision',
    'Hlt2ExoticaRHNuDecision',
    'Hlt2ExoticaRHNuHighMassDecision',
    'Hlt2ForwardDecision',
    'Hlt2JetsDiJetDecision',
    'Hlt2JetsDiJetLowPtDecision',
    'Hlt2JetsDiJetMVLowPtDecision',
    'Hlt2JetsDiJetMuMuDecision',
    'Hlt2JetsDiJetMuMuLowPtDecision',
    'Hlt2JetsDiJetSVDecision',
    'Hlt2JetsDiJetSVLowPtDecision',
    'Hlt2JetsDiJetSVMuDecision',
    'Hlt2JetsDiJetSVMuLowPtDecision',
    'Hlt2JetsDiJetSVSVDecision',
    'Hlt2JetsDiJetSVSVLowPtDecision',
    'Hlt2JetsJetLowPtDecision',
    'Hlt2JetsJetMuLowPtDecision',
    'Hlt2JetsJetSVLowPtDecision',
    'Hlt2LowMultChiC2HHDecision',
    'Hlt2LowMultChiC2HHHHDecision',
    'Hlt2LowMultChiC2HHHHWSDecision',
    'Hlt2LowMultChiC2HHWSDecision',
    'Hlt2LowMultChiC2PPDecision',
    'Hlt2LowMultChiC2PPWSDecision',
    'Hlt2LowMultD2K3PiDecision',
    'Hlt2LowMultD2K3PiWSDecision',
    'Hlt2LowMultD2KKPiDecision',
    'Hlt2LowMultD2KKPiWSDecision',
    'Hlt2LowMultD2KPiDecision',
    'Hlt2LowMultD2KPiPiDecision',
    'Hlt2LowMultD2KPiPiWSDecision',
    'Hlt2LowMultD2KPiWSDecision',
    'Hlt2LowMultDiElectronDecision',
    'Hlt2LowMultDiElectron_noTrFiltDecision',
    'Hlt2LowMultDiMuonDecision',
    'Hlt2LowMultDiMuon_PSDecision',
    'Hlt2LowMultDiPhotonDecision',
    'Hlt2LowMultDiPhoton_HighMassDecision',
    'Hlt2LowMultHadron_noTrFiltDecision',
    'Hlt2LowMultL2pPiDecision',
    'Hlt2LowMultL2pPiWSDecision',
    'Hlt2LowMultLMR2HHDecision',
    'Hlt2LowMultLMR2HHHHDecision',
    'Hlt2LowMultLMR2HHHHWSDecision',
    'Hlt2LowMultLMR2HHWSDecision',
    'Hlt2LowMultMuonDecision',
    'Hlt2LowMultPi0Decision',
    'Hlt2LowMultTechnical_MinBiasDecision',
    'Hlt2LumiDecision',
    'Hlt2MBNoBiasDecision',
    'Hlt2MajoranaBLambdaMuDDDecision',
    'Hlt2MajoranaBLambdaMuLLDecision',
    'Hlt2NoBiasNonBeamBeamDecision',
    'Hlt2PassThroughDecision',
    'Hlt2PhiBs2PhiPhiDecision',
    'Hlt2PhiIncPhiDecision',
    'Hlt2PhiPhi2KsKsDecision',
    'Hlt2PhiPhi2KsKsD0CtrlDecision',
    'Hlt2RadiativeB2GammaGammaDecision',
    'Hlt2RadiativeB2GammaGammaDDDecision',
    'Hlt2RadiativeB2GammaGammaDoubleDecision',
    'Hlt2RadiativeB2GammaGammaLLDecision',
    'Hlt2RadiativeBd2KstGammaDecision',
    'Hlt2RadiativeBd2KstGammaULUnbiasedDecision',
    'Hlt2RadiativeBs2PhiGammaDecision',
    'Hlt2RadiativeBs2PhiGammaUnbiasedDecision',
    'Hlt2RadiativeHypb2L0HGammaOmDecision',
    'Hlt2RadiativeHypb2L0HGammaOmEEDecision',
    'Hlt2RadiativeHypb2L0HGammaXiDecision',
    'Hlt2RadiativeHypb2L0HGammaXiEEDecision',
    'Hlt2RadiativeIncHHGammaDecision',
    'Hlt2RadiativeIncHHGammaEEDecision',
    'Hlt2RadiativeIncHHHGammaDecision',
    'Hlt2RadiativeIncHHHGammaEEDecision',
    'Hlt2RadiativeLb2L0GammaEELLDecision',
    'Hlt2RadiativeLb2L0GammaLLDecision',
    'Hlt2RareCharmD02EMuDecision',
    'Hlt2RareCharmD02KKMuMuDecision',
    'Hlt2RareCharmD02KKMueDecision',
    'Hlt2RareCharmD02KKeeDecision',
    'Hlt2RareCharmD02KMuDecision',
    'Hlt2RareCharmD02KPiDecision',
    'Hlt2RareCharmD02KPiMuMuDecision',
    'Hlt2RareCharmD02KPiMuMuSSDecision',
    'Hlt2RareCharmD02KPiMueDecision',
    'Hlt2RareCharmD02KPieeDecision',
    'Hlt2RareCharmD02MuMuDecision',
    'Hlt2RareCharmD02PiPiDecision',
    'Hlt2RareCharmD02PiPiMuMuDecision',
    'Hlt2RareCharmD02PiPiMueDecision',
    'Hlt2RareCharmD02PiPieeDecision',
    'Hlt2RareCharmD2KEEOSDecision',
    'Hlt2RareCharmD2KEESSDecision',
    'Hlt2RareCharmD2KEEWSDecision',
    'Hlt2RareCharmD2KEMuOSDecision',
    'Hlt2RareCharmD2KMuEOSDecision',
    'Hlt2RareCharmD2KMuESSDecision',
    'Hlt2RareCharmD2KMuEWSDecision',
    'Hlt2RareCharmD2KMuMuOSDecision',
    'Hlt2RareCharmD2KMuMuSSDecision',
    'Hlt2RareCharmD2KMuMuWSDecision',
    'Hlt2RareCharmD2PiEEOSDecision',
    'Hlt2RareCharmD2PiEESSDecision',
    'Hlt2RareCharmD2PiEEWSDecision',
    'Hlt2RareCharmD2PiEMuOSDecision',
    'Hlt2RareCharmD2PiMuEOSDecision',
    'Hlt2RareCharmD2PiMuESSDecision',
    'Hlt2RareCharmD2PiMuEWSDecision',
    'Hlt2RareCharmD2PiMuMuOSDecision',
    'Hlt2RareCharmD2PiMuMuSSDecision',
    'Hlt2RareCharmD2PiMuMuWSDecision',
    'Hlt2RareCharmLc2PMuMuDecision',
    'Hlt2RareCharmLc2PMuMuSSDecision',
    'Hlt2RareCharmLc2PMueDecision',
    'Hlt2RareCharmLc2PeeDecision',
    'Hlt2RareStrangeKPiMuMuDecision',
    'Hlt2RareStrangeKPiMuMuSSDecision',
    'Hlt2RareStrangeSigmaPMuMuDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2SingleMuonHighPTDecision',
    'Hlt2SingleMuonLowPTDecision',
    'Hlt2SingleMuonRareDecision',
    'Hlt2SingleMuonVHighPTDecision',
    'Hlt2StrangeLFVMuonElectronSoftDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoE2BodyDecision',
    'Hlt2TopoE3BodyDecision',
    'Hlt2TopoE4BodyDecision',
    'Hlt2TopoEE2BodyDecision',
    'Hlt2TopoEE3BodyDecision',
    'Hlt2TopoEE4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2TopoMuE2BodyDecision',
    'Hlt2TopoMuE3BodyDecision',
    'Hlt2TopoMuE4BodyDecision',
    'Hlt2TopoMuMu2BodyDecision',
    'Hlt2TopoMuMu3BodyDecision',
    'Hlt2TopoMuMu4BodyDecision',
    'Hlt2TopoMuMuDDDecision',
    'Hlt2TransparentDecision',
    'Hlt2TriMuonDetachedDecision',
    'Hlt2TriMuonTau23MuDecision',
    'Hlt2XcMuXForTauB2XcFakeMuDecision',
    'Hlt2XcMuXForTauB2XcMuDecision',
    'Hlt2ErrorEventDecision',
    'Hlt2GlobalDecision'
    'L0B1gasDecision',
    'L0B2gasDecision',
    'L0CALODecision',
    'L0DiEM,lowMultDecision',
    'L0DiHadron,lowMultDecision',
    'L0DiMuonDecision',
    'L0DiMuon,lowMultDecision',
    'L0DiMuonNoSPDDecision',
    'L0ElectronDecision',
    'L0Electron,lowMultDecision',
    'L0ElectronHiDecision',
    'L0ElectronNoSPDDecision',
    'L0HadronDecision',
    'L0HadronNoSPDDecision',
    'L0HighSumETJetDecision',
    'L0MUON,minbiasDecision',
    'L0MuonDecision',
    'L0Muon,lowMultDecision',
    'L0MuonNoSPDDecision',
    'L0NoPVFlagDecision',
    'L0PhotonDecision',
    'L0Photon,lowMultDecision',
    'L0PhotonHiDecision',
    'L0PhotonNoSPDDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TrackMVALooseDecision',
    'Hlt1TwoTrackMVALooseDecision',
    'Hlt1TrackMuonDecision',
    'Hlt1DiMuonHighMassDecision',
    'Hlt1DiMuonLowMassDecision',
    'Hlt1SingleMuonHighPTDecision',
    'Hlt1DiMuonNoL0Decision',
    'Hlt1B2GammaGammaDecision',
    'Hlt1B2HH_LTUNB_KKDecision',
    'Hlt1B2HH_LTUNB_KPiDecision',
    'Hlt1B2HH_LTUNB_PiPiDecision',
    'Hlt1B2PhiGamma_LTUNBDecision',
    'Hlt1B2PhiPhi_LTUNBDecision',
    'Hlt1BeamGasBeam1Decision',
    'Hlt1BeamGasBeam1VeloOpenDecision',
    'Hlt1BeamGasBeam2Decision',
    'Hlt1BeamGasBeam2VeloOpenDecision',
    'Hlt1BeamGasHighRhoVerticesDecision',
    'Hlt1Bottomonium2KstarKstarDecision',
    'Hlt1Bottomonium2PhiPhiDecision',
    'Hlt1CalibHighPTLowMultTrksDecision',
    'Hlt1CalibMuonAlignJpsiDecision',
    'Hlt1CalibRICHMirrorRICH1Decision',
    'Hlt1CalibRICHMirrorRICH2Decision',
    'Hlt1CalibTrackingKKDecision',
    'Hlt1CalibTrackingKPiDecision',
    'Hlt1CalibTrackingKPiDetachedDecision',
    'Hlt1CalibTrackingPiPiDecision',
    'Hlt1DiMuonNoIPDecision',
    'Hlt1DiProtonDecision',
    'Hlt1DiProtonLowMultDecision',
    'Hlt1IncPhiDecision',
    'Hlt1L0AnyDecision',
    'Hlt1L0AnyNoSPDDecision',
    'Hlt1LowMultDecision',
    'Hlt1LowMultMaxVeloCutDecision',
    'Hlt1LowMultPassThroughDecision',
    'Hlt1LowMultVeloAndHerschel_HadronsDecision',
    'Hlt1LowMultVeloCut_HadronsDecision',
    'Hlt1LowMultVeloCut_LeptonsDecision',
    'Hlt1LumiDecision',
    'Hlt1MBNoBiasDecision',
    'Hlt1MultiDiMuonNoIPDecision',
    'Hlt1MultiMuonNoL0Decision',
    'Hlt1NoBiasNonBeamBeamDecision',
    'Hlt1ODINTechnicalDecision',
    'Hlt1SingleElectronNoIPDecision',
    'Hlt1SingleMuonNoIPDecision',
    'Hlt1Tell1ErrorDecision',
    'Hlt1VeloClosingMicroBiasDecision',
    'Hlt1ErrorEventDecision',
    'Hlt1GlobalDecision'
]

def IsoConfig(branch):
    '''
    Configuration of vertex isolation tuple tools.
    '''
    from Configurables import TupleToolVtxIsolnVelo, TupleToolVtxIsolnPlus
    TupleToolVeloIso = TupleToolVtxIsolnVelo(name="TupleToolVeloIso")
    branch.addTool(TupleToolVeloIso)
    branch.ToolList += ["TupleToolVtxIsolnVelo/TupleToolVeloIso"]
    # TupleToolVeloIso.OutputLevel = VERBOSE
    TupleToolCharmIso = TupleToolVtxIsolnPlus(name="TupleToolCharmIso")
    TupleToolCharmIso.InputParticles = ["/Event/Phys/StdLoosePions",
                                        "/Event/Phys/StdLooseKaons",
                                        "/Event/Phys/StdLooseMuons",
                                        "/Event/Phys/StdLooseElectrons",
                                        "/Event/Phys/StdLooseProtons"]
    # TupleToolCharmIso.OutputLevel = VERBOSE
    branch.addTool(TupleToolCharmIso)
    branch.ToolList += ["TupleToolVtxIsolnPlus/TupleToolCharmIso"]


def DTFConfig(branch, particle):
    '''
    Configuration of DecayTreeFitter variables.
    '''
    # Start by adding the Dict2Tuple to the master branch -
    # this will write the values we are going to retrieve into the ntuple
    DictTuple = branch.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")

    # We need a DecayTreeFitter. DTFDict will provide the fitter and the connection to the tool chain
    # we add it as a source of data to the Dict2Tuple
    DictTuple.addTool(DTFDict,"DTF")
    DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
    DictTuple.NumVar = 20     # reserve a suitable size for the dictionaire

    # configure the DecayTreeFitter in the usual way
    DictTuple.DTF.constrainToOriginVertex = True
    DictTuple.DTF.daughtersToConstrain = ["{0}".format(particle)]

    # Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
    # these functors will be applied to the refitted(!) decay tree
    # they act as a source to the DTFDict
    DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict")
    DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

    DictTuple.DTF.dict.Variables = {
        "DTFDict_M": "M",
        "DTFDict_P": "P",
        "DTFDict_PT": "PT",
        "DTFDict_PX": "PX",
        "DTFDict_PY": "PY",
        "DTFDict_PZ": "PZ",
        "DTFDict_PXERR": "PCOV2(0,0)",
        "DTFDict_PYERR": "PCOV2(1,1)",
        "DTFDict_PZERR": "PCOV2(2,2)",
        "DTFDict_VX": "VFASPF(VX)",
        "DTFDict_VY": "VFASPF(VY)",
        "DTFDict_VZ": "VFASPF(VZ)",
        "DTFDict_VXERR": "PCOV2(4,4)",
        "DTFDict_VYERR": "PCOV2(5,5)",
        "DTFDict_VZERR": "PCOV2(6,6)",
       }


def LifetimeConfig(branch):
    '''
    Configuration of lifetime tool for the tau.
    '''
    LokiToolLTime = LoKi__Hybrid__TupleTool('LokiToolLTime')
    LokiToolLTime.Variables = {
        "LOKI_DTF_CTAU": "DTF_CTAU( 'tau+' == ABSID , True )",
        "LOKI_DTF_CTAUS": "DTF_CTAUSIGNIFICANCE( 'tau+' == ABSID , True )",
        "LOKI_DTF_CTAUERR": "DTF_CTAUERR( 'tau+' == ABSID , True )",
    }
    branch.addTool(LokiToolLTime)
    branch.ToolList += ['LoKi::Hybrid::TupleTool/LokiToolLTime']


def DalitzConfig(branch):
    '''
    Configuration of Dalitz variables for the 3pi (M12, M23, ... variables)
    '''
    from Configurables import TupleToolDalitzAdv
    TupleToolDAdv = TupleToolDalitzAdv(name="TupleToolDAdv")
    TupleToolDAdv.NoSameSign = False
    TupleToolDAdv.NoNeutrals = False
    branch.addTool(TupleToolDAdv)
    branch.ToolList += ["TupleToolDalitzAdv/TupleToolDAdv"]


def KinematicFitConfig(branch, isMC):
    from Configurables import TupleToolB2XTauNuFit, BTauFitter
    tupleToolB2XtaunuFit = TupleToolB2XTauNuFit(name='tupleToolB2XtaunuFit')
    tupleToolB2XtaunuFit.UseMinuit = False
    tupleToolB2XtaunuFit.IsMC = isMC
    # tupleToolB2XtaunuFit.OutputLevel= VERBOSE
    branch.addTool(tupleToolB2XtaunuFit)
    branch.ToolList += ["TupleToolB2XTauNuFit/tupleToolB2XtaunuFit"]


def FinalStateConfig(branch):
    LokiToolFS = LoKi__Hybrid__TupleTool('LokiToolFS')
    LokiToolFS.Variables = {
        "LOKI_ISBASIC": "switch(ISBASIC,1,0)",
        "LOKI_HASTRACK": "switch(HASTRACK,1,0)",
        "LOKI_HASRICH": "switch(HASRICH,1,0)",
        "LOKI_ISMUON": "switch(ISMUON,1,0)",
        "LOKI_HASMUON": "switch(HASMUON,1,0)",
        "LOKI_CLONEDIST": "CLONEDIST",
        "LOKI_PIDK": "PIDK",
        "LOKI_PIDe": "PIDe",
        "LOKI_PIDpi": "PIDpi",
        "LOKI_PIDp": "PIDp",
        "LOKI_PIDmu": "PIDmu",
        "LOKI_TRCHI2": "TRCHI2",
        "LOKI_TRPCHI2": "TRPCHI2",
        "LOKI_TRCHI2DOF": "TRCHI2DOF",
        "LOKI_TRGHP": "TRGHP",
        "LOKI_TRLH": "TRLH",
    }
    branch.addTool(LokiToolFS)
    branch.ToolList += ['LoKi::Hybrid::TupleTool/LokiToolFS']


def ParentConfig(branch):
    LokiToolParent = LoKi__Hybrid__TupleTool('LokiToolParent')
    LokiToolParent.Variables = {
        "LOKI_VFASPF_VCHI2": "VFASPF(VCHI2)",
        "LOKI_VFASPF_VDOF": "VFASPF(VDOF)",
        "LOKI_VFASPF_VCHI2VDOF": "VFASPF(VCHI2/VDOF)",
        # "LOKI_VMINDVSOURCE" : "VFASPF(VMINDVSOURCE(VSOURCEDV(PRIMARY)))",
        # # "LOKI_MINDVSOURCE" : "MINDVSOURCE(VSOURCEDV(PRIMARY))",
        "LOKI_VMINVDDV": "VFASPF(VMINVDDV(PRIMARY))",
        "LOKI_MINVDDV": "MINVDDV(PRIMARY)",
        "LOKI_VFASPF_VPCHI2": "VFASPF(VPCHI2)",
        "LOKI_MM": "MM",
        "LOKI_M": "M",
        "LOKI_BPVLTFITCHI2": "BPVLTFITCHI2('PropertimeFitter/properTime:PUBLIC')",
        "LOKI_BPVLTCHI2": "BPVLTCHI2('PropertimeFitter/properTime:PUBLIC')",
        "LOKI_BPVLTIME": "BPVLTIME('PropertimeFitter/properTime:PUBLIC')",
        "LOKI_BPVVDR": "BPVVDR",
        "LOKI_BPVVDZ": "BPVVDZ",
        "LOKI_BPVVDCHI2": "BPVVDCHI2",
        "LOKI_BPVDIRA":  "BPVDIRA",
        "LOKI_DOCAMAX": "DOCAMAX",
        "LOKI_DOCACHI2MAX": "DOCACHI2MAX",
        "LOKI_CORRM": "BPVCORRM"
    }
    branch.addTool(LokiToolParent)
    branch.ToolList += ['LoKi::Hybrid::TupleTool/LokiToolParent']


def CommonConfig(branch):
    LokiToolCommon = LoKi__Hybrid__TupleTool('LokiToolCommon')
    LokiToolCommon.Variables = {
        "LOKI_ETA": "ETA",
        "LOKI_Y": "Y",
        "LOKI_ABSID": "ABSID",
        "LOKI_BPVIPCHI2": "BPVIPCHI2()",
        "LOKI_ID": "ID",
        "LOKI_MIPDV_PRIMARY": "MIPDV(PRIMARY)",
        "LOKI_MIPCHI2DV_PRIMARY": "MIPCHI2DV(PRIMARY)",
        "LOKI_P": "P",
        "LOKI_PT": "PT"
    }
    branch.addTool(LokiToolCommon)
    branch.ToolList += ['LoKi::Hybrid::TupleTool/LokiToolCommon']


def TISTOSToolConfig(branch, run="run1"):
    '''
    Trigger information configuration using list of triggers defined above.
    '''
    TupleToolTISTOSParent = TupleToolTISTOS(name='TupleToolTISTOSParent')
    if run is "run1":
        TupleToolTISTOSParent.TriggerList = triggers_run1
    elif run is "run2":
        TupleToolTISTOSParent.TriggerList = triggers_run2
    else:
        raise ValueError('wrong value assigned to parameter run !')
    TupleToolTISTOSParent.VerboseL0 = True
    TupleToolTISTOSParent.VerboseHlt1 = True
    TupleToolTISTOSParent.VerboseHlt2 = True
    TupleToolTISTOSParent.Verbose = True
    TupleToolTISTOSParent.OutputLevel = outputLevel
    branch.addTool(TupleToolTISTOSParent)
    branch.ToolList += ["TupleToolTISTOS/TupleToolTISTOSParent"]


def TupleToolConfig(decaytreetuple):
    decaytreetuple.ToolList += [
        "TupleToolEventInfo",
        "TupleToolRecoStats",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolTrackInfo",
        "TupleToolEWTrackIsolation",
    ]

    decaytreetuple.addTool(TupleToolGeometry)
    decaytreetuple.TupleToolGeometry.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolRecoStats)
    decaytreetuple.TupleToolRecoStats.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolKinematic)
    decaytreetuple.TupleToolKinematic.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolPid)
    decaytreetuple.TupleToolPid.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolPrimaries)
    decaytreetuple.TupleToolPrimaries.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolTrackInfo)
    decaytreetuple.TupleToolTrackInfo.Verbose = True
    decaytreetuple.TupleToolTrackInfo.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolEWTrackIsolation)
    decaytreetuple.TupleToolEWTrackIsolation.MinConeRadius = .1
    decaytreetuple.TupleToolEWTrackIsolation.ConeStepSize = .1
    decaytreetuple.TupleToolEWTrackIsolation.ExtreParticlesLocation = "StdAllLoosePions"
    decaytreetuple.TupleToolEWTrackIsolation.OutputLevel = outputLevel


def TupleToolMCConfig(decaytreetuple):
    decaytreetuple.ToolList += [
        "TupleToolMCTruth",
        "TupleToolEventInfo",
        "TupleToolMCBackgroundInfo",
        "TupleB2XMother",
    ]

    decaytreetuple.addTool(TupleToolMCTruth)
    decaytreetuple.TupleToolMCTruth.OutputLevel = outputLevel
    decaytreetuple.TupleToolMCTruth.ToolList = [
        "MCTupleToolKinematic",
        "MCTupleToolHierarchy"
    ]
    decaytreetuple.TupleToolMCTruth.addTool(MCTupleToolKinematic)
    decaytreetuple.TupleToolMCTruth.MCTupleToolKinematic.OutputLevel = outputLevel
    decaytreetuple.TupleToolMCTruth.addTool(MCTupleToolHierarchy)
    decaytreetuple.TupleToolMCTruth.MCTupleToolHierarchy.OutputLevel = outputLevel
    decaytreetuple.addTool(TupleToolMCBackgroundInfo)
    decaytreetuple.TupleToolMCBackgroundInfo.OutputLevel = outputLevel


def TupleToolIsoGenericConfig(decaytreetuple):
    '''
    Configuration of Basem's TupleTool to isolate charged tracks
    '''

    TupleToolIsoGeneric = decaytreetuple.addTupleTool("TupleToolIsoGeneric")
    TupleToolIsoGeneric.Verbose = True

    MCTruth_noniso = TupleToolMCTruth('MCTruth_noniso')
    MCTruth_noniso.ToolList += ["MCTupleToolHierarchy"]
    LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
    LoKiTool_noniso.Variables = {
        "ETA": "ETA",
        "PHI": "PHI"
        }
    TupleToolIsoGeneric.ToolList += ["TupleToolTrackInfo",
                                     "LoKi::Hybrid::TupleTool/LoKiTool_noniso",
                                     "TupleToolMCTruth/MCTruth_noniso"
                                     ]
    TupleToolIsoGeneric.addTool(LoKiTool_noniso)
    TupleToolIsoGeneric.addTool(MCTruth_noniso)
    decaytreetuple.TupleToolIsoGeneric.OutputLevel = outputLevel
