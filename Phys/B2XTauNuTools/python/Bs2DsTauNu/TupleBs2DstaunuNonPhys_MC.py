###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBs2DstaunuNonPhys = GaudiSequencer("SeqTupleBs2DstaunuNonPhys")
TupleBs2DstaunuNonPhys = DecayTreeTuple("Bs2DstaunuNonPhysTuple")
TupleBs2DstaunuNonPhys.OutputLevel = outputLevel
TupleBs2DstaunuNonPhys.Inputs = ["Phys/Bs2DsTauNuNonPhysTauForB2XTauNu/Particles"]

TupleBs2DstaunuNonPhys.Decay = "[(Bs -> ^(D_s- -> ^K- ^K+ ^pi-) ^(tau+ -> ^pi+ ^pi+ ^pi+ ))]CC"
TupleBs2DstaunuNonPhys.Branches = {
    "Bs": "[(Bs -> (D_s- -> K- K+ pi-) (tau+ -> pi+ pi+ pi+))]CC",
    "Ds": "[(Bs -> ^(D_s- -> K- K+ pi-) (tau+ -> pi+ pi+ pi+))]CC",
    "ds_km": "[(Bs -> (D_s- -> ^K- K+ pi-) (tau+ -> pi+ pi+ pi+))]CC",
    "ds_kp": "[(Bs -> (D_s- -> K- ^K- pi-) (tau+ -> pi+ pi+ pi+))]CC",
    "ds_pion": "[(Bs -> (D_s- -> K- K+ ^pi-) (tau+ -> pi+ pi+ pi+))]CC",
    "tau": "[(Bs -> (D_s- -> K- K+ pi-) ^(tau+ -> pi+ pi+ pi+))]CC",
    "tau_pion": "[(Bs -> (D_s- -> K- K+ pi-) (tau+ -> ^pi+ ^pi+ ^pi+))]CC"
}

TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='Bs')
TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='Ds')
TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='ds_km')
TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='ds_kp')
TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='ds_pion')
TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='tau')
TupleBs2DstaunuNonPhys.addTool(TupleToolDecay, name='tau_pion')

FinalStateConfig(TupleBs2DstaunuNonPhys.ds_km)
FinalStateConfig(TupleBs2DstaunuNonPhys.ds_kp)
FinalStateConfig(TupleBs2DstaunuNonPhys.ds_pion)
FinalStateConfig(TupleBs2DstaunuNonPhys.tau_pion)

DalitzConfig(TupleBs2DstaunuNonPhys.tau)
DalitzConfig(TupleBs2DstaunuNonPhys.Ds)

IsoConfig(TupleBs2DstaunuNonPhys.tau)
IsoConfig(TupleBs2DstaunuNonPhys.Bs)
# LifetimeConfig(TupleBs2DstaunuNonPhys.tau)
ParentConfig(TupleBs2DstaunuNonPhys.tau)
ParentConfig(TupleBs2DstaunuNonPhys.Ds)
ParentConfig(TupleBs2DstaunuNonPhys.Bs)

CommonConfig(TupleBs2DstaunuNonPhys.Bs)
CommonConfig(TupleBs2DstaunuNonPhys.Ds)
CommonConfig(TupleBs2DstaunuNonPhys.ds_kp)
CommonConfig(TupleBs2DstaunuNonPhys.ds_km)
CommonConfig(TupleBs2DstaunuNonPhys.ds_pion)
CommonConfig(TupleBs2DstaunuNonPhys.tau)
CommonConfig(TupleBs2DstaunuNonPhys.tau_pion)
# Kinematic fit is using TupleToolB2XTauNuFit
# which is not used in the analysis
# which causes bugs with Bs2Dstaunu events
#KinematicFitConfig(TupleBs2DstaunuNonPhys.S.Bs,isMC)

DTFConfig(TupleBs2DstaunuNonPhys.Bs, 'D_s-')

TISTOSToolConfig(TupleBs2DstaunuNonPhys.Bs)
TupleToolConfig(TupleBs2DstaunuNonPhys)

TupleToolIsoGenericConfig(TupleBs2DstaunuNonPhys)

# Specific MC tools
TupleToolMCConfig(TupleBs2DstaunuNonPhys)

GaudiSequencer("SeqTupleBs2DstaunuNonPhys").Members.append(TupleBs2DstaunuNonPhys)
SeqTupleBs2DstaunuNonPhys.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBs2DstaunuNonPhys").Members.append(SeqTupleBs2DstaunuNonPhys)

