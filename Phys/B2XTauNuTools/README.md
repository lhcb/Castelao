### B2XTauNuTools Package

#### Dedicated to the creation of Data and MC Ntuples for all the hadronic semitauonic analyses.

#### Organisation of the package

The package organization is as follows:

├── BDT_xmls  
├── cmt  
├── doc  
├── forDecayTreeTuple  
├── options  
├── python  
│   ├── B2XTauNuTools  
│   ├── Bd2DstTauNu  
│   ├── Bd2DTauNu  
│   ├── Bu2D0TauNu  
│   └── Lb2LcTauNu  
├── scripts  
└── src  

- forDecayTreeTuple is the folder whith all the TupleTools .h and .cpp files which should be copied to Phys/DecayTreeTuple/src
in order to compile locally the package.

- python/B2XTauNuTools is the location with the generic scripts:
    - `SelB2XTauNu_ConfigureStripping.py` is needed to run the dedicated stripping lines on MC DSTs.
    - `TupleHelper.py` is used to define all the TupleTools configurations for DecayTreeTuple in the DaVinci script.

- python/XXXTauNu are all the dedicated folders for each semitauonic analysis.


For instance in Lb2LcTauNu, one can find:
- `TupleLb2LctauNu.py` is the script to declare the DecayTreeTuple looking for Lb0 -> Lc+ (-> pKpi) tau-(->pi-pi+pi-) nu_tau decay
- The same version with `_MC` exists to load specific MCTupleTools.
- `NonPhys` and `WS` versions are also there to study combinatorial background.


The DaVinci script is called `Lb2Lctaunu_strp.py`, some options (SIMULATION, CONDDB, ...) are not yet described and will be filled by the ganga script.
- The `_TEST.py` version of the script is there for local tests.


The `Lb2Lctaunu_ganga.py` script is there to launch jobs on the grid.
- it uses a dedicated class `JobConf` to store the Name, the Polarity, the year of Data taking, the bookeeping location of the dataset and the related tags for each job.
- for each job, a dedicated script `Lb2Lctaunu_strp_name_polarity.py` will be created.
- To launch jobs one can either do :  
    - `ganga Lb2Lctaunu_ganga.py --all` to launch all possible jobs   
    - `ganga Lb2Lctaunu_ganga.py --jobs Data16 Data15` to launch jobs for Data16 and Data16 (for both polarity)
    - `ganga Lb2Lctaunu_ganga.py --jobs Data16 --polarity Up` to only launch a job for Data16 Up 

The script `setup_DaVinci.py` can be used to automatically setup a local
DaVinci with B2XTauNu package:
`python setup_DaVinci.py --name XXX --version v41r2` will create the folder
`DaVinciDev_XXX` and process to collect all the needed files and then starts
the compilation.

To install manually the package, one has to create a DaVinci environment:
`lb-dev DaVinci/v44r6`
Then install the different required packages:
```
git lb-use DaVinci
git lb-use Analysis
git lb-use Castelao

git lb-checkout DaVinci/v44r6 Phys/DaVinci
git lb-checkout Analysis/v20r6 Phys/DecayTreeTuple
git lb-checkout Analysis/v20r6 Phys/DecayTreeTupleMC
git lb-checkout Analysis/v20r6 Phys/DaVinciTrackScaling

git lb-checkout Castelao/v1r3 Phys/B2XTauNuTools
```

The files located in `Phys/B2XTauNuTools/forDecayTreeTuple` should be copied to
`Phys/DecayTreeTuple/src` and the file `forDecayTreeTuple_CMakeLists.txt`
should becone `Phys/DecayTreeTuple/CMakeLists.txt`

Finally `make configure && make -j8` should complete the process.
