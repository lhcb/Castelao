/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TupleToolDalitzAdv.cpp,v 1.1 2012-04-03 09:50:34 elsasser Exp $
// Include files

// local
#include "TupleToolDalitzAdv.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

#include "Kernel/IParticleTransporter.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include <boost/lexical_cast.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolDalitzAdv
//
// 2012-04-03 : Christian Elsasser
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_COMPONENT( TupleToolDalitzAdv )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  TupleToolDalitzAdv::TupleToolDalitzAdv( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
    : TupleToolBase ( type, name , parent )
    , m_transporter()
    , m_transporterName ("ParticleTransporter:PUBLIC")
{
    declareInterface<IParticleTupleTool>(this);

    declareProperty( "Transporter", m_transporterName );
    
    declareProperty( "NoSameSign",m_suppressSameSign = true,
                    "Not including same sign combination (also two neutrals)" );
    declareProperty( "NoNeutrals",m_suppressNeutrals = true,
                    "Not including neutral particles" );
}

//=============================================================================

StatusCode TupleToolDalitzAdv::initialize()
{
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_transporter = tool<IParticleTransporter>(m_transporterName, this);

  return sc;
}

//=============================================================================

StatusCode TupleToolDalitzAdv::fill( const LHCb::Particle* /*mother*/
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);

  bool test = true;
  if( P )
  {
      
      // Write down the daughters
      const SmartRefVector< LHCb::Particle > pDaughters = P->daughters();
      LHCb::IParticlePropertySvc* m_ppSvc;
      m_ppSvc = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc", true);
      
      // Check number of daughters
      int nDauts = 0;
      for (SmartRefVector< LHCb::Particle >::const_iterator jdau = pDaughters.begin() ; jdau != pDaughters.end() ; ++jdau) {
          nDauts++;
          if (msgLevel(MSG::VERBOSE) ) {
              verbose() << "Particle is " << m_ppSvc->find((*jdau)->particleID())->name() << endmsg;
              verbose() << "Charge of particle: " << (*jdau)->charge() << endmsg;
          }
      }
      
      if ( 2>=nDauts ){
          return Warning("Will not fill Dalitz of two body decay "+prefix,StatusCode::SUCCESS,0);
      }else{
          if (msgLevel(MSG::VERBOSE) ) {
              info() << prefix << " consists of " << nDauts << " daughter particles." << endmsg;
          }
      }
      
      // Loop over all daughter particles (twice) and combine the 4-momentum to calculate the invariant mass
      int nFirst = 0;
      for (SmartRefVector< LHCb::Particle >::const_iterator jdau = pDaughters.begin() ; jdau != pDaughters.end() ; ++jdau) {
          nFirst++;
          int nSecond = nFirst-1;
          const LHCb::ProtoParticle* protoFirst = (*jdau)->proto();
          for (SmartRefVector< LHCb::Particle >::const_iterator idau = jdau ; idau != pDaughters.end() ; ++idau) {
              nSecond++;
              const LHCb::ProtoParticle* protoSecond = (*idau)->proto();
              // Check if the two particles are not the same
              if (protoFirst!=protoSecond) {
                  // Skip same sign combination (if not wanted)
                  if (m_suppressSameSign && ((*idau)->charge()==(*jdau)->charge())) {
                      continue;
                  }
                  // Skip combination with neutrals (if not wanted)
                  if (m_suppressNeutrals && ((*idau)->charge()==0 || (*jdau)->charge()==0)) {
                      continue;
                  }
                  Gaudi::LorentzVector pFirst  = (*jdau)->momentum();
                  Gaudi::LorentzVector pSecond = (*idau)->momentum();
                  Gaudi::LorentzVector pComb   = pFirst + pSecond;
                  test &= tuple->column( prefix+"_M"+boost::lexical_cast<std::string>( nFirst )+boost::lexical_cast<std::string>( nSecond ),pComb.M());
                  if (msgLevel(MSG::VERBOSE) ) {
                      verbose() << "Combination of " << m_ppSvc->find((*jdau)->particleID())->name() << " and " << m_ppSvc->find((*idau)->particleID())->name() << "has invariant mass " << pComb.M() << " MeV/c^2." << endmsg;
                  }
              }
          }
          
          
      }

  }
  else
  {
    return StatusCode::FAILURE;
  }
  return StatusCode(test);
}

//=============================================================================

/*double TupleToolDalitzAdv::preFitMass(const LHCb::Particle* p) const 
{
  Gaudi::LorentzVector Mom ;
  for ( SmartRefVector< LHCb::Particle >::const_iterator d = p->daughters().begin();
        d != p->daughters().end() ; ++d){
    Mom += (*d)->momentum();
  }
  return Mom.M() ;
}*/
