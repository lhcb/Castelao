# The CASTELAO project

Urania is, together with [Urania](https://gitlab.cern.ch/lhcb/Urania), a high-level physics analysis software repository (or collection of packages used by various working groups) for the LHCb experiment, based on [DaVinci](https://gitlab.cern.ch/lhcb/DaVinci). Documentation can be found at [this link](https://lhcbdoc.web.cern.ch/lhcbdoc/castelao/). 

## Use this project from the nightlies

```
lb-run -c best --nightly lhcb-head Castelao/HEAD bash
```

or

```
lb-run -c best --nightly lhcb-run2-patches Castelao/run2-patches bash
```
## Tips and tricks

* <b>Solving merge conflicts</b>: please have a look to the [documentation](https://lhcb-core-doc.web.cern.ch/lhcb-core-doc/Development.html#merge-conflict-in-gitlab) (cloning the whole repository for this task is acceptable).

## Mailing list

For issues, questions and problems; please send an e-mail to:

[<b>lhcb-castelao-developers@cern.ch</b>](mailto:lhcb-castelao-developers@cern.ch)

## About 

Alfonso Daniel Rodríguez Castelao (30 January 1886 – 7 January 1950), commonly known as Castelao, was a Galician writer, painter, doctor and politician. He was one of the most important promoters of the Galician identity and culture, and was one of the main names behind the cultural and literary movement _Xeración Nós_. His alma mater was the University of Santiago de Compostela.  [(+ info)](https://en.wikipedia.org/wiki/Alfonso_Daniel_Rodr%C3%ADguez_Castelao). 

