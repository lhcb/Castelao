###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

'''Options to make ntuples for Lb->(Lc->pKpi)pi from Stripping output. On MC, 
ntuples are made for the original candidates plus an identical ntuple for 
candidates refitted with the 2017 VELO error parametrisation from real data.

Tested on data from:
/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST

and MC from:
/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/15364010/LB2LCH.STRIP.DST
'''

from Configurables import DaVinci
from PhysSelPython.Wrappers import StrippingData, SelectionSequence, TupleSelection
from StrippingDoc import StrippingDoc
from VeloErrorsStudy.refit import add_simcond_overlay, \
    refit_tracks_and_particles, configure_originals_mc_truth

mc = DaVinci().getProp('Simulation')

data = StrippingData('StrippingLb2LcPiLc2PKPiBeauty2CharmLine', ('Lb2LcH.Strip' if mc else None))
doc = StrippingDoc('Stripping29')
linedoc = doc.get_line('StrippingLb2LcPiLc2PKPiBeauty2CharmLine')
decay = linedoc.tuple_config()['Decay']


def make_tuple(name, inputdata, mc, isoriginals = False):
    '''Make the ntuple with the given name from the input selection.
    If mc = True, add MC truth tools. If isoriginals = True,
    configure MC truth for the copied, non-refitted particles.'''
    dtt = TupleSelection(name, [inputdata], Decay = decay)
    dtf = dtt.addTupleTool('TupleToolDecayTreeFitter')
    dtf.Verbose = True
    tttr = dtt.addTupleTool('TupleToolTrackInfo')
    tttr.Verbose = True
    if mc:
        dtt.ToolList += ['TupleToolMCBackgroundInfo']
        mctools = ['MCTupleToolKinematic', 'MCTupleToolPrompt']
        if not isoriginals:
            ttmc = dtt.addTupleTool('TupleToolMCTruth')
            ttmc.ToolList = mctools
        else:
            configure_originals_mc_truth(dtt.algorithm(), mctools)

    seq = SelectionSequence(name + '_Seq', TopSelection = dtt)
    return seq.sequence()


if mc:
    DaVinci().StrippingStream = 'Lb2LcH.Strip'
    # Add the overlay for the VELO error parametrisation from real data.
    add_simcond_overlay()
    # Get the sequence to refit the tracks & particles with the updated
    # VELO error parametrisation, and selections for the original,
    # non-refitted particles.
    refitseq, originals = refit_tracks_and_particles(data, DaVinci().getRootInTES(), mc)
    originals = originals[0]
    DaVinci().UserAlgorithms += [refitseq, make_tuple('Lb2LcpiTuple_refit', data, mc)]
else:
    DaVinci().StrippingStream = 'Bhadron'
    originals = data

DaVinci().UserAlgorithms += [make_tuple('Lb2LcpiTuple', originals, mc, True)]
DaVinci().TupleFile = 'Tuples.root'

