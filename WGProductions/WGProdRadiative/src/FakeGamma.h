/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EXTRAS_FAKEGAMMA_H
#define EXTRAS_FAKEGAMMA_H 1

// Include files
// from DaVinci.
#include "CaloUtils/CaloParticle.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/DaVinciAlgorithm.h"
#include <memory>

/** @class FakeGamma FakeGamma.h Extras/FakeGamma.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2013-07-19
 *
 *  2021-02-08 : Jiahui Zhuo (Adaptation and modernization)
 */

class FakeGamma : public DaVinciAlgorithm {
public:
  /// Standard constructor
  FakeGamma( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~FakeGamma(); ///< Destructor

  virtual StatusCode initialize() override; ///< Algorithm initialization
  virtual StatusCode execute() override;    ///< Algorithm execution

protected:
private:
  bool                 addShift( LHCb::Particle* particle, bool smearable ) const;
  int                  m_PhotonID;
  unsigned int         m_pid;
  mutable IRndmGenSvc* m_rndmSvc; ///< random number service
  IRndmGenSvc*         rndmSvc() const { return m_rndmSvc; }
  std::string          m_part;
  bool                 m_smear;
  double               m_a;
  double               m_b;
  double               m_shift;
};
#endif // EXTRAS_FAKEGAMMA_H
