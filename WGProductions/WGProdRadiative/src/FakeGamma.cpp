/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "FakeGamma.h"
#include "Event/Particle.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FakeGamma
//
// Apply Ecal post-calibration to photons and direct mother
//
// Warning : update particle momentum only (not yet propagated to covariance)
//
// 2013-07-19 : Olivier Deschamps
// 2021-02-08 : Jiahui Zhuo (Adaptation and modernization)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FakeGamma )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FakeGamma::FakeGamma( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ), m_PhotonID( 22 ), m_pid( 0 ), m_rndmSvc( 0 ) {
  declareProperty( "Particle", m_part = "J/psi(1S)" ); // fake gamma with J/psi
  declareProperty( "Smear", m_smear = false );         // energy smear to reproduce calo resolution
  declareProperty( "a", m_a = 0.11 );                  // stochastic term
  declareProperty( "b", m_b = 0.02 );                  // constant term
  declareProperty( "Shift", m_shift = 1000000. );
}
//=============================================================================
// Destructor
//=============================================================================
FakeGamma::~FakeGamma() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode FakeGamma::initialize() {
  StatusCode sc = DaVinciAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  m_rndmSvc                           = svc<IRndmGenSvc>( "RndmGenSvc", true );
  LHCb::IParticlePropertySvc*   ppsvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  const LHCb::ParticleProperty* pp    = ppsvc->find( m_part );
  if ( !pp ) return Error( "Unknown particle " + m_part, StatusCode::FAILURE );
  m_pid = pp->pdgID().abspid();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FakeGamma::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  setFilterPassed( true );

  const LHCb::Particle::Range parts = particles();
  int                         ng    = 0;
  int                         npart = 0;
  for ( LHCb::Particle::Range::const_iterator iparts = ( parts ).begin(); iparts != ( parts ).end(); ++iparts ) {
    LHCb::Particle* part = (LHCb::Particle*)*iparts;
    if ( part->particleID().abspid() != m_pid ) continue;
    npart++;
    if ( addShift( part, true ) ) {
      ng++;
      // part -> setParticleID( LHCb::ParticleID (m_PhotonID) ); // no - it's confuse the OfflineVertexFitter (fakeGamma
      // is classified as Flying !)
      LHCb::Particle::ConstVector daughters = part->daughtersVector();
      for ( LHCb::Particle::ConstVector::const_iterator id = daughters.begin(); id != daughters.end(); id++ ) {
        LHCb::Particle* daughter = (LHCb::Particle*)*id;
        addShift( daughter, false );
      }
    }
  }
  counter( "Input " + m_part ) += npart;
  counter( "Fake Gamma created" ) += ng;
  setFilterPassed( true ); // Mandatory. Set to true if event is accepted.
  return StatusCode::SUCCESS;
}

bool FakeGamma::addShift( LHCb::Particle* particle, bool smearable ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Shift the covariance matrix here " << endmsg;
  if ( !particle ) return false;

  if ( m_shift == 0. ) return true;

  if ( particle->info( LHCb::Particle::HasBremAdded, 0. ) == 1. ) {
    counter( particle->particleID().toString() + " is already shifted" ) += 1;
    return false;
  }
  // Shift covariance of the particle
  debug() << "Before : " << particle->posCovMatrix() << endmsg;
  Gaudi::SymMatrix3x3& cov = (Gaudi::SymMatrix3x3&)particle->posCovMatrix();
  cov( 0, 0 ) += m_shift;
  cov( 1, 1 ) += m_shift;
  debug() << "After : " << particle->posCovMatrix() << endmsg;

  particle->addInfo( LHCb::Particle::HasBremAdded, 1. ); // abuse brem flag !!
  counter( "Covariance shift added to " + particle->particleID().toString() ) += 1;

  // reproduce (approximatively) the calo resolution ??
  if ( smearable && m_smear ) {
    double                e = particle->momentum().E();
    double                s = std::sqrt( pow( m_a / std::sqrt( e ), 2 ) + pow( m_b, 2 ) ) * e;
    Rndm::Numbers         shot( rndmSvc(), Rndm::Gauss( 0.0, s ) );
    Gaudi::LorentzVector& mom = (Gaudi::LorentzVector&)particle->momentum();
    mom *= ( 1 + shot );
    counter( "Momentum smearing added to " + particle->particleID().toString() ) += 1;
    particle->setMeasuredMass( mom.M() );
  }

  // propagate shift to track States (for DTF)
  const LHCb::ProtoParticle* proto = particle->proto();
  if ( !proto ) return true;
  LHCb::Track* track = const_cast<LHCb::Track*>( proto->track() );
  if ( !track ) return true;
  std::vector<LHCb::State*> theStates = track->states();
  for ( std::vector<LHCb::State*>::const_iterator iter = theStates.begin(); iter != theStates.end(); ++iter ) {
    ( *iter )->covariance()( 0, 0 ) += m_shift;
    ( *iter )->covariance()( 1, 1 ) += m_shift;
  } // loop over the states
  return true;
}
