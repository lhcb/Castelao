###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
configurePIDCalibTupleProduction(
      DataType = "2015"
      , TupleFile = "tuple.root"
      , Simulation = False
      , Lumi       = False
      , Stream     = "Turbo"
      , InputType = 'DST'
      , EvtMax    = 10000
      , mdstOutputFile = "PID"
      , mdstOutputPrefix = "00000"
      , sTableFile = "../../sTables/sPlotTables-15MagDown.root"
   )

