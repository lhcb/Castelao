###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import ROOT

def fit ( hist
          , model
          , method = None # "Simplex", "Migrad"...
          , minimizer = None # "Minuit"...
          , hesse = True
          , minos = False
          , save = True
          , strategy = 1
          , printLevel = 0
        ):
  
  minimizer = minimizer if minimizer  else "Minuit"
  method    = method    if method     else "Simplex"

  return model.fitTo ( hist
              , ROOT.RooFit.Minimizer   ( minimizer, method )
              , ROOT.RooFit.Hesse       ( hesse )
              , ROOT.RooFit.Minos       ( minos )
              , ROOT.RooFit.Save        ( save )
              , ROOT.RooFit.Strategy    ( strategy )
              , ROOT.RooFit.PrintLevel  ( printLevel )
        )
          
