/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ErasePidVariables.h
#ifndef ERASEPIDVARIABLES_H
#define ERASEPIDVARIABLES_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciTupleAlgorithm.h"


 class ErasePidVariables : public DaVinciTupleAlgorithm {
 public:
   ErasePidVariables( const std::string& name, ISvcLocator* pSvcLocator );

   virtual ~ErasePidVariables( );

   StatusCode initialize() override;
   StatusCode execute   () override;
   StatusCode finalize  () override;

 protected:

 private:
   std::string m_protoPath;
   bool m_eraseRich, m_eraseMuon, m_eraseComb;
 };
#endif // ERASEPIDVARIABLES_H
