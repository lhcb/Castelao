/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

/// GaudiAlg
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

/// Kernel
#include "Kernel/ILHCbMagnetSvc.h"

/// Event
#include "Event/RecSummary.h"
#include "Event/ODIN.h"
#include "Event/L0DUReport.h"
#include "Event/HltDecReports.h"

/// local
#include "EvtTupleToolPIDCalib.h"

//-----------------------------------------------------------------------------
// Implementation file for class EvtTupleToolPIDCalib: 
//
// 2013-09-26 : Philip Hunt
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( EvtTupleToolPIDCalib )
   
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
EvtTupleToolPIDCalib::EvtTupleToolPIDCalib( const std::string& type, 
                                            const std::string& name,
                                            const IInterface* parent )
  : TupleToolBase ( type, name, parent )
{
  declareInterface<IEventTupleTool>(this);
}

//=============================================================================
// Standard initalize method
//=============================================================================
StatusCode EvtTupleToolPIDCalib::initialize() 
{
   const StatusCode sc = TupleToolBase::initialize();
   if ( sc.isFailure() ) return sc;
   m_magSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
   return sc;
}

//=============================================================================
// Destructor
//=============================================================================
EvtTupleToolPIDCalib::~EvtTupleToolPIDCalib() {}

//=============================================================================
// Main method
//=============================================================================
StatusCode EvtTupleToolPIDCalib::fill( Tuples::Tuple& tup )
{
  const std::string prefix = fullName();

  // Load the ODIN
  const LHCb::ODIN* odin;
  odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default);
  if ( !odin ) { 
    odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default,false);
  }
  if ( !odin )
  {
    // should always be available ...
    return Error( "Cannot load the ODIN data object", StatusCode::SUCCESS );
  }

  // Load the RecSummary object
  const LHCb::RecSummary * rS;
  rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
  if ( !rS ) 
  {
    rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false);
  }

  // Fill the tuple
  bool test = true;

  // RecSummary info
  int nPVs=-2;
  int nTracks=-2;
  int nLongTracks=-2;
  int nDownTracks=-2;
  int nMuonTracks=-2;
  int nRich1Hits=-2;
  int nRich2Hits=-2;
  int nSPDHits=-2;  
    
  // Verbose RecSummary info
  int nUpTracks=-2;
  int nVeloTracks=-2;
  int nTTracks=-2;
  int nBackTracks=-2;
  int nVeloClusters=-2;
  int nITClusters=-2;
  int nTTClusters=-2;
  int nOTClusters=-2;
  int nMuonCoordsS0=-2;
  int nMuonCoordsS1=-2;
  int nMuonCoordsS2=-2;
  int nMuonCoordsS3=-2;
  int nMuonCoordsS4=-2;

  if (rS) {
    // Get RecSummary info
    nPVs = rS->info(LHCb::RecSummary::nPVs,-1);
    
    nTracks = rS->info(LHCb::RecSummary::nTracks,-1);
    nLongTracks = rS->info(LHCb::RecSummary::nLongTracks,-1);
    nDownTracks = rS->info(LHCb::RecSummary::nDownstreamTracks,-1);
  
    nRich1Hits = rS->info(LHCb::RecSummary::nRich1Hits,-1);
    nRich2Hits = rS->info(LHCb::RecSummary::nRich2Hits,-1);
    nSPDHits = rS->info(LHCb::RecSummary::nSPDhits,-1);
        
    if (isVerbose())
    {
      // get extra RecSummary information
      nUpTracks = rS->info(LHCb::RecSummary::nUpstreamTracks,-1);
      nVeloTracks = rS->info(LHCb::RecSummary::nVeloTracks,-1);
      nTTracks = rS->info(LHCb::RecSummary::nTTracks,-1);
      nBackTracks = rS->info(LHCb::RecSummary::nBackTracks,-1);
      
      nVeloClusters = rS->info(LHCb::RecSummary::nVeloClusters,-1);
      nITClusters = rS->info(LHCb::RecSummary::nITClusters,-1);
      nTTClusters = rS->info(LHCb::RecSummary::nTTClusters,-1);
      nOTClusters = rS->info(LHCb::RecSummary::nOTClusters,-1);
      nMuonCoordsS0 = rS->info(LHCb::RecSummary::nMuonCoordsS0,-1);
      nMuonCoordsS1 = rS->info(LHCb::RecSummary::nMuonCoordsS1,-1);
      nMuonCoordsS2 = rS->info(LHCb::RecSummary::nMuonCoordsS2,-1);
      nMuonCoordsS3 = rS->info(LHCb::RecSummary::nMuonCoordsS3,-1);
      nMuonCoordsS4 = rS->info(LHCb::RecSummary::nMuonCoordsS4,-1);
    }
  }

  // Fill magnet polarity
  test &= tup->column( prefix+"Polarity", (short)(m_magSvc->isDown()?-1:1) );

  // Fill ODIN info
  test &= tup->column( prefix+"runNumber", odin->runNumber() );
  test &= tup->column( prefix+"eventNumber", odin->eventNumber() );
  test &= tup->column( prefix+"OdinTCK", odin->triggerConfigurationKey() );

  // Fill RecSumary info
  test &= tup->column( prefix+"nPVs", nPVs );
    
  test &= tup->column( prefix+"nTracks", nTracks );
  test &= tup->column( prefix+"nLongTracks", nLongTracks );
  test &= tup->column( prefix+"nDownstreamTracks", nDownTracks );
  test &= tup->column( prefix+"nMuonTracks", nMuonTracks );

  test &= tup->column( prefix+"nRich1Hits", nRich1Hits );
  test &= tup->column( prefix+"nRich2Hits", nRich2Hits );
   
  test &= tup->column( prefix+"nSPDHits", nSPDHits );
  
  if (isVerbose())
  {
    // Fill extra ODIN information
    test &= tup->column( prefix+"BCID", odin->bunchId() );
    test &= tup->column( prefix+"BCType", odin->bunchCrossingType() );
    test &= tup->column( prefix+"GpsTime", odin->gpsTime() );

    // Get the L0 DU report
    LHCb::L0DUReport* report =
      getIfExists<LHCb::L0DUReport>(evtSvc(),LHCb::L0DUReportLocation::Default);
    if ( !report )
    {
      report =
        getIfExists<LHCb::L0DUReport>(evtSvc(),LHCb::L0DUReportLocation::Default,false);
    }
    if ( !report )
    {
      Warning( "Can't get LHCb::L0DUReportLocation::Default (" +
               LHCb::L0DUReportLocation::Default + ")" ).ignore();
    }

    // Get the HLT decision reports
    LHCb::HltDecReports* decreport =
      getIfExists<LHCb::HltDecReports>(evtSvc(),LHCb::HltDecReportsLocation::Default);
    if ( !decreport )
    {
      decreport =
        getIfExists<LHCb::HltDecReports>(evtSvc(),LHCb::HltDecReportsLocation::Default,false);
    }
    if ( !decreport )
    {
      Warning( "Can't get LHCb::HltDecReportsLocation::Default (" +
               LHCb::HltDecReportsLocation::Default + ")" ).ignore();
    }

    // Fill the L0 and HLT TCKs
    test &= tup->column( prefix+"L0DUTCK", report ? report->tck() : 0 );
    test &= tup->column( prefix+"HLTTCK", decreport ? decreport->configuredTCK() : 0 );

    // Fill extra RecSummary info
    test &= tup->column( prefix+"nUpstreamTracks", nUpTracks );
    test &= tup->column( prefix+"nVeloTracks", nVeloTracks );
    test &= tup->column( prefix+"nTTracks", nTTracks );
    test &= tup->column( prefix+"nBackTracks", nBackTracks );
    
    test &= tup->column( prefix+"nVeloClusters", nVeloClusters );
    test &= tup->column( prefix+"nITClusters", nITClusters );
    test &= tup->column( prefix+"nTTClusters", nTTClusters );
    test &= tup->column( prefix+"nOTClusters", nOTClusters );
    
    test &= tup->column( prefix+"nMuonCoordsS0", nMuonCoordsS0 );
    test &= tup->column( prefix+"nMuonCoordsS1", nMuonCoordsS1 );
    test &= tup->column( prefix+"nMuonCoordsS2", nMuonCoordsS2 );
    test &= tup->column( prefix+"nMuonCoordsS3", nMuonCoordsS3 );
    test &= tup->column( prefix+"nMuonCoordsS4", nMuonCoordsS4 );
  }
  return StatusCode(test);
}
