#!/bin/bash
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
export KEY=$1
export TMP_FILELIST='file.tmp'


case $KEY in
  "CaloPID" ) export TMP_HISTO="~/cmtuser/CastelaoDev_v2r1/Rootfiles/histo_CaloPID_1718.root";;

  *) echo "'"$KEY "' not known as key";;
esac

export PYTHONPATH=../python:$PYTHONPATH
python sPlotNeutral.py $KEY $TMP_HISTO
